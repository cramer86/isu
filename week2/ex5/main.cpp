#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
using namespace std;
main()
{
  struct timespec start;
  double time;
  if( clock_gettime( CLOCK_REALTIME, &start) == -1 ) {
    perror( "clock gettime" );
    exit( EXIT_FAILURE );
  }
  time = start.tv_sec;
  printf("the time since 1970: %lf\n", time);
  return 0;
}


