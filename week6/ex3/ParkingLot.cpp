#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <iostream>
using namespace std;

void *entryThread(void *id);
void *exitThread(void *id);
void *parking(void *id);
void openGate();
void closeGate();
int cars = 20;
int parkingLot = 0, carsAtEntry = 0, carsAtExit = 0, upperLimit = 2;

bool entryGate = false, exitGate = false;

pthread_mutex_t mutexEntry, mutexExit;
pthread_cond_t condEntry, condExit;

struct car {
  int id;
  int wait;
};

void openGate() {
  sleep(1);
}

void closeGate() {
  sleep(1);
}

void *entryThread(void *id)
{
  printf("Entry gate watcher initialized\n");
  
  while(true) {
    pthread_mutex_lock(&mutexEntry);
    while(!carsAtEntry && (parkingLot < upperLimit))
      pthread_cond_wait(&condEntry, &mutexEntry);
    openGate();
    entryGate = true;
    parkingLot++;
    printf("cars currently in the parking lot: %i\n", parkingLot);
    pthread_cond_broadcast(&condEntry);
    while(carsAtEntry)
      pthread_cond_wait(&condEntry, &mutexEntry);
    closeGate();
    entryGate = false;
    pthread_mutex_unlock(&mutexEntry);

    //guard is sleepy
  }
}

void *exitThread(void *id)
{
  printf("Exit gate watcher initialized\n");

  while(true) {
    pthread_mutex_lock(&mutexExit);
    while(!carsAtExit)
      pthread_cond_wait(&condExit, &mutexExit);
    openGate();
    exitGate = true;
    parkingLot--;
    printf("cars currently in the parking lot: %i\n", parkingLot);
    pthread_cond_broadcast(&condExit);
    while(carsAtExit)
      pthread_cond_wait(&condExit, &mutexExit);
    closeGate();
    exitGate = false;
    pthread_mutex_unlock(&mutexExit);
    
    //guard is sleepy
    sleep(2);
  }
}

void *parking(void *id)
{
  car * audi = (car*)id;
  //sleeping to create a delay
  sleep(1);
  
  //==========================
  //try to enter parking lot
  //==========================

  
  pthread_mutex_lock(&mutexEntry);
  cout << "car: " << audi->id << " at the entry gate" << endl;
  carsAtEntry++;

  pthread_cond_broadcast(&condEntry);
  while(!entryGate)
    pthread_cond_wait(&condEntry, &mutexEntry);
  carsAtEntry--;

  pthread_cond_broadcast(&condEntry);
  pthread_mutex_unlock(&mutexEntry);  

  //shopping
  sleep(3);
  
  //==========================
  //leaving the parking lot
  //==========================

  pthread_mutex_lock(&mutexExit);

  cout << "car: " << audi->id << " leaving parking lot" << endl;
  
  carsAtExit++;

  pthread_cond_broadcast(&condExit);
  while(!exitGate)
    pthread_cond_wait(&condExit, &mutexExit);
  carsAtExit--;

  printf("cars currently in the parking lot: %i\n", parkingLot);

  pthread_cond_broadcast(&condExit);
  pthread_mutex_unlock(&mutexExit); 

  return 0;
}

int main()
{
  //init mutex
  pthread_mutex_init(&mutexEntry, NULL);
  pthread_mutex_init(&mutexExit, NULL);
  
  //init conds
  pthread_cond_init(&condEntry, NULL);
  pthread_cond_init(&condExit, NULL);

  //creating threads
  pthread_t entryGuard, exitGuard;
  
  pthread_create(&entryGuard, NULL, entryThread, NULL);
  pthread_create(&exitGuard, NULL, exitThread, NULL);
  
  
  //creating car threads
  pthread_t carThreads[cars];
  car golf[cars];
  
  for(int i = 0; i < cars; i++)
    {  
      golf[i].id = i;
      golf[i].wait = (i%5) + 1;
      
      pthread_create(&carThreads[i], NULL, parking, &golf[i]);
      usleep(100);
    }
  
  //finishing threads
  pthread_join(entryGuard, NULL);
  pthread_join(exitGuard, NULL);

  for(int i = 0; i < cars; i++) {
    pthread_join(carThreads[i], NULL);
  }

  //clean up
  pthread_mutex_destroy(&mutexEntry);
  pthread_mutex_destroy(&mutexExit);
  pthread_cond_destroy(&condEntry);
  pthread_cond_destroy(&condExit);
  return 0;
}
