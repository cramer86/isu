#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <iostream>
using namespace std;

void *entryThread(void *id);
void *exitThread(void *id);
void *parking(void *id);
void openGate();
void closeGate();
int cars = 1;
int parkingLot = 0, carsAtEntry = 0, carsAtExit = 0;

bool entryGate = false, exitGate = false;

pthread_mutex_t mutexEntry, mutexExit;
pthread_cond_t condEntry, condExit;

void openGate() {
  sleep(1);
}

void closeGate() {
  sleep(1);
}

void *entryThread(void *id)
{
  printf("Entry gate watcher initialized\n");
  
  pthread_mutex_lock(&mutexEntry);
  while(!carsAtEntry)
    pthread_cond_wait(&condEntry, &mutexEntry);
  openGate();
  entryGate = true;
  pthread_cond_broadcast(&condEntry);
  while(carsAtEntry)
    pthread_cond_wait(&condEntry, &mutexEntry);
  closeGate();
  entryGate = false;
  pthread_mutex_unlock(&mutexEntry);
}

void *exitThread(void *id)
{
  printf("Exit gate watcher initialized\n");

  pthread_mutex_lock(&mutexExit);
  while(!carsAtExit)
    pthread_cond_wait(&condExit, &mutexExit);
  openGate();
  exitGate = true;
  pthread_cond_broadcast(&condExit);
  while(carsAtExit)
    pthread_cond_wait(&condExit, &mutexExit);
  closeGate();
  exitGate = false;
  pthread_mutex_unlock(&mutexExit);
}

void *parking(void *id)
{
  //sleeping to create a delay
  sleep(1);
  
  //==========================
  //try to enter parking lot
  //==========================

  cout << "entering parking lot" << endl;
  pthread_mutex_lock(&mutexEntry);
  
  carsAtEntry++;

  pthread_cond_broadcast(&condEntry);
  while(!entryGate)
    pthread_cond_wait(&condEntry, &mutexEntry);
  carsAtEntry--;
  parkingLot++;

  pthread_cond_broadcast(&condEntry);
  pthread_mutex_unlock(&mutexEntry);

  printf("cars currently in the parking lot: %i\n", parkingLot);
  

  //shopping
  sleep(3);
  
  //==========================
  //leaving the parking lot
  //==========================
  cout << "leaving parking lot" << endl;
  pthread_mutex_lock(&mutexExit);
  
  carsAtExit++;

  pthread_cond_broadcast(&condExit);
  while(!exitGate)
    pthread_cond_wait(&condExit, &mutexExit);
  carsAtExit--;
  parkingLot--;
  pthread_cond_broadcast(&condExit);
  pthread_mutex_unlock(&mutexExit); 
  
  return 0;
}

int main()
{
  //init mutex
  pthread_mutex_init(&mutexEntry, NULL);
  pthread_mutex_init(&mutexExit, NULL);
  
  //init conds
  pthread_cond_init(&condEntry, NULL);
  pthread_cond_init(&condExit, NULL);

  //creating threads
  pthread_t entryGuard, exitGuard, car;
  
  pthread_create(&entryGuard, NULL, entryThread, NULL);
  pthread_create(&exitGuard, NULL, exitThread, NULL);
  pthread_create(&car, NULL, parking, NULL);
  
  //finishing threads
  pthread_join(entryGuard, NULL);
  pthread_join(exitGuard, NULL);
  pthread_join(car, NULL);

  //clean up
  pthread_mutex_destroy(&mutexEntry);
  pthread_mutex_destroy(&mutexExit);
  pthread_cond_destroy(&condEntry);
  pthread_cond_destroy(&condExit);
  return 0;
}
