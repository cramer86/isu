entryguard:
while(!carAtEntry)
  wait;
openEntry();
count++;

exitguard:
while(!carAtExit)
  wait;
openExit();
count--;
