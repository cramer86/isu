#include "Scope.hpp"

Scope::Scope(pthread_mutex_t *t)
{
  lock = t;

  pthread_mutex_lock(lock);
}

Scope::~Scope()
{
  pthread_mutex_unlock(lock);
}
