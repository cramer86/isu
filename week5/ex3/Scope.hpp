#include <pthread.h>

class Scope
{
public: 
  //constructor
  Scope(pthread_mutex_t *t);
  //destroyer
  ~Scope();

private:
  pthread_mutex_t *lock;
};
