#include <iostream>
#include <boost/shared_ptr.hpp>

using namespace std;

int main ( int argc , char * argv [])
{
  boost::shared_ptr<string> ss(new string("hello world"));
  cout << "String length : " << *ss << std::endl;
  
  cout << "count at: " << ss.use_count() << std::endl;

  {
    boost::shared_ptr<string> ss2 = ss;
    cout << "created new string ss2: " << *ss2 << std::endl;
    cout << "count in scope at: " << ss2.use_count() << std::endl;

    boost::shared_ptr<string> ss3(ss);
    cout << "created new string ss3: " << *ss3 << std::endl;
    cout << "count in scope at: " << ss3.use_count() << std::endl;
  }
  cout << "count out of scope is: " << ss.use_count() << std::endl;
  return 0;
}
