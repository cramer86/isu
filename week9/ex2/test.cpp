#include <iostream>

#include "smartstring.hpp" 

int main ( int argc , char * argv [])
{
  SmartString ss(new std::string("Hello world"));
  std::cout << "String length : " << ss->length () << std::endl;
  
  std::cout << "count at: " << ss.getCount() << std::endl;

  {
    SmartString ss2 = ss;
    std::cout << "created new string ss2: " << *ss2 << std::endl;
    std::cout << "count in scope at: " << ss2.getCount() << std::endl;

    SmartString ss3(ss);
    std::cout << "created new string ss3: " << *ss3 << std::endl;
    std::cout << "count in scope at: " << ss3.getCount() << std::endl;
  }
  std::cout << "count out of scope is: " << ss.getCount() << std::endl;
  return 0;
}
