#ifndef SMARTSTRING_H
#define SMARTSTRING_H

#include <string>
using namespace std;

class SmartString
{
public:
  SmartString(std::string* str_);
  ~SmartString();
  string* get();
  string* operator->();
  string& operator*();
  SmartString(const SmartString& other);
  SmartString& operator=(const SmartString& other);
  unsigned int getCount() const;
private:
  string* str;
  unsigned int* count;
};

#endif // SMARTSTRING_H
