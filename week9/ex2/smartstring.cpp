#include "smartstring.hpp" 

SmartString::SmartString(string* str_)
{
    str = str_;
    count = new unsigned int;
    *count = 1;
}

SmartString::~SmartString()
{
  (*count)--;
  if(count == 0) {
    delete str;
    delete count;
  }    
}

SmartString::SmartString(const SmartString& other)
{
  str = other.str;
  count = other.count;
  (*count)++;
}

string* SmartString::get()
{
  
  return str;
}

string* SmartString::operator->()
{
  return str;
}

string& SmartString::operator*()
{
  return *str;
}

SmartString& SmartString::operator=(const SmartString& other)
{
  if(this == &other)
    return *this;
  else {
    str = other.str;
    count = other.count;
    (*count)++;
    return *this;
  }
}

unsigned int SmartString::getCount() const
{
  return *count;
}
