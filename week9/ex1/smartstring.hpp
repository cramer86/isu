#ifndef SMARTSTRING_H
#define SMARTSTRING_H

#include <string>
using namespace std;
class SmartString
{
public:
    SmartString(std::string* str_);
    ~SmartString();
    string* get();
    string* operator->();
    string& operator*();
private:
    SmartString(const SmartString&);
    SmartString& operator=(const SmartString&);
    string* str;
};

#endif // SMARTSTRING_H
