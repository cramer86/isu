#include "smartstring.hpp" 

SmartString::SmartString(string* str_)
{
    str = str_;
}

SmartString::~SmartString()
{
    delete str;
}

string* SmartString::get()
{
    return str;
}

string* SmartString::operator->()
{
    return str;
}

string& SmartString::operator*()
{
    return *str;
}
