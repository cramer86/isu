#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <iostream>
#include "MsgQueue.hpp"
using namespace std;

void *entryThread(void *id);
void *exitThread(void *id);
void *parking(void *id);
void entryHandler(Message* msg, unsigned long id);
void exitHandler(Message* msg, unsigned long id);
void parkingHandler(Message* msg, unsigned long id);
bool running = true;
int cars = 20;
int parkingLot = 0, carsAtEntry = 0, carsAtExit = 0, parkingLotMax = 5;

MsgQueue * entryQueue = new MsgQueue(5);
MsgQueue * exitQueue = new MsgQueue(5);

struct car {
  int id;
  int wait;
};


struct carMsg : public Message
{
  MsgQueue * mq;
  car * carId;
};

struct stdMsg : public Message {};

enum
  {
    REQ_ENTRY = 0,
    REQ_EXIT = 1,
    CFM_ENTRY = 2,
    CFM_EXIT = 3,
    CAR_CFM_ENTRY = 4,
    CAR_CFM_EXIT = 5,
    CAR_FULL = 6
  };


void entryHandler(Message* msg, unsigned long id)
{
  carMsg * bil = static_cast<carMsg*>(msg);
  switch(id) {
  case REQ_ENTRY:
    {
      carMsg * resp;
      resp = new carMsg;
      resp->carId = bil->carId;
      if(parkingLot >= parkingLotMax) {
	bil->mq->send(CAR_FULL, resp);
      } else {
	cout << "car: " << bil->carId->id << " wants to enter" << endl;
	MsgQueue * que = bil->mq;
	que->send(CFM_ENTRY, resp);
	cout << "car: " << bil->carId->id << " has been promitted to enter " << endl;
	parkingLot++;
	cout << "currently " << parkingLot << " cars in the lot" << endl;
      }
      break;
    }
  }
}

void exitHandler(Message* msg, unsigned long id)
{
  carMsg * bil = static_cast<carMsg*>(msg);
  switch(id) {
  case REQ_EXIT:
    {
      carMsg * resp;
      resp = new carMsg;
      resp->carId = bil->carId;
      cout << "car: " << bil->carId->id << " wants to leave" << endl;
      MsgQueue * que = bil->mq;
      que->send(CFM_EXIT, resp);
      cout << "car: " << bil->carId->id << " has been promitted to leave " << endl;
      parkingLot--;
      cout << "currently " << parkingLot << " cars in the lot" << endl;
      break;
    }
  }
}

void parkingHandler(Message* msg, unsigned long id)
{

  carMsg * bil = static_cast<carMsg*>(msg);

  // car wants to enter
  switch(id) {
  case CFM_ENTRY:
    {
      cout << "car: " << bil->carId->id << " has entered the parking lot" << endl;
      sleep(bil->carId->wait);
      carMsg * exit;
      exit = new carMsg;
      exit->carId = bil->carId;
      exitQueue->send(REQ_EXIT, exit);
      break;
    }
  case CAR_FULL:
    {
      cout << "car: " << bil->carId->id << " has not been allowed, parkinglot is full" << endl;
      sleep(bil->carId->wait);
      cout << "car: " << bil->carId->id << " retrying to enter parkinglot" << endl;
      carMsg * entry;
      entry = new carMsg;
      entry->carId = bil->carId;
      entryQueue->send(REQ_ENTRY, entry);
      break;
    }
  case CFM_EXIT:
    {
      cout << "car: " << bil->carId->id << " has left the parkinglot" << endl;
      return;
    }
  }
}



void *entryThread(void *id)
{
  printf("Entry gate watcher initialized\n");

  while ( running )
    {
      unsigned long id;
      Message * msg = entryQueue->receive (id);
      entryHandler (msg , id);
      delete msg ;
    }
}

void *exitThread(void *id)
{
  printf("Exit gate watcher initialized\n");

  while ( running )
    {
      unsigned long id;
      Message * msg = exitQueue->receive (id);
      exitHandler (msg , id);
      delete msg ;
    }
}

void *parking(void *id)
{
  car * carId = (car*)id;

  MsgQueue * mq = new MsgQueue(5);
  
  //ask to enter
  carMsg * msgEnter;
  msgEnter = new carMsg;

  msgEnter->mq = mq;
  msgEnter->carId = carId;

  entryQueue->send(REQ_ENTRY, msgEnter);

  while ( running )
    {
      unsigned long id;
      Message * msg = mq->receive(id);
      parkingHandler (msg , id);
      delete msg ;
    }
}

int main()
{
  //creating threads
  pthread_t entryGuard, exitGuard;
  
  pthread_create(&entryGuard, NULL, entryThread, NULL);
  pthread_create(&exitGuard, NULL, exitThread, NULL);
  
  
  //creating car threads
  pthread_t carThreads[cars];
  car golf[cars];
  
  for(int i = 0; i < cars; i++)
    {  
      golf[i].id = i;
      golf[i].wait = (i%5) + 1;
      
      pthread_create(&carThreads[i], NULL, parking, &golf[i]);
    }
  
  //finishing threads
  pthread_join(entryGuard, NULL);
  pthread_join(exitGuard, NULL);

  for(int i = 0; i < cars; i++) {
    pthread_join(carThreads[i], NULL);
  }

  //clean up
  pthread_mutex_destroy(&mutexEntry);
  pthread_mutex_destroy(&mutexExit);
  pthread_cond_destroy(&condEntry);
  pthread_cond_destroy(&condExit);
  return 0;
}
