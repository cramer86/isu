#include "MsgQueue.hpp"
#include <pthread.h>
pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;

MsgQueue::MsgQueue(unsigned long maxSize)
{
  size = maxSize; // setting the size
}

void MsgQueue::send(unsigned long id, Message* msg)
{
  pthread_mutex_lock(&lock);
  //if the queue is full
  while(queue.size() >= size)
    {
      pthread_cond_wait(&cond, &lock);
    }
  
  //create and add an item to the queue
  msgItem item;
  item.item = msg;
  item.id = id;
  queue.push(item);
  
  pthread_mutex_unlock(&lock);
  pthread_cond_broadcast(&cond);
}

Message* MsgQueue::receive(unsigned long& id)
{
  pthread_mutex_lock(&lock);
  while(queue.empty())
    {
      pthread_cond_wait(&cond, &lock);
    }
  //get first message in queue
  msgItem item = queue.front();
  
  //add id to ptr
  id = item.id;

  //remove it from the queue
  queue.pop();
  
  pthread_mutex_unlock(&lock);
  pthread_cond_broadcast(&cond);

  return item.item;
}
MsgQueue::~MsgQueue()
{
  
}
