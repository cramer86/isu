#include <cstddef>
#include "Message.hpp"
#include <queue>

using namespace std;

struct msgItem
{
  Message* item;
  unsigned long id;
};

class MsgQueue
{
private:
  unsigned long size;
  std::queue<msgItem> queue;
public:
  MsgQueue(unsigned long maxSize);
  void send(unsigned long id, Message* msg = NULL);
  Message* receive(unsigned long& id);
  ~MsgQueue();
};
