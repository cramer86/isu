#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <iostream>
#include "MsgQueue.hpp"
using namespace std;

MsgQueue *msgQueue = NULL;

enum
  {
    ID_POINT = 0
  };

struct Point3D : public Message
{
  int x;
  int y;
  int z;
};

void handler(Message* msg, unsigned long id)
{
  switch(id) {
  case ID_POINT:
    {
      Point3D* point = static_cast<Point3D*>(msg);
      cout << "got point: x: " << point->x << " ,y: " << point->y << " , z: " << point->z << endl;
      break;
    }
 default:
   cout << "default" << endl;
  }
}

void *sender(void* ptr)
{
  while(true)
    {
      Point3D* point = new Point3D;
      point->x = 1;
      point->y = 2;
      point->z = 3;
      msgQueue->send(ID_POINT, point);
      
      sleep(1);
    }
}

void *reciever(void* ptr)
{
  while(true)
    {
      unsigned long id;
      Message* msg = msgQueue->receive(id);
      handler(msg, id);
      delete msg;
    }
}

int main()
{
  msgQueue = new MsgQueue(3);
  pthread_t sender_thread;
  pthread_t reciever_thread;

  pthread_create(&sender_thread, NULL, sender, NULL); 
  pthread_create(&reciever_thread, NULL, reciever, NULL);
  pthread_join(sender_thread, NULL);
  pthread_join(reciever_thread, NULL);
  return 0;
}
