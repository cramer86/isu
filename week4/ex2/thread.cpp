#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

void *reader( void *ptr );
void *incrementer( void *ptr );

unsigned int shared = 0;

main()
{
     pthread_t thread1, thread2;
     const char *id0 = "0";
     const char *id1 = "1";
     int  iret1, iret2;

     iret1 = pthread_create( &thread1, NULL, reader, (void*) id0);
     iret2 = pthread_create( &thread2, NULL, incrementer, (void*) id1);

     pthread_join( thread1, NULL);
     pthread_join( thread2, NULL); 
     exit(0);
}

void *reader( void *ptr )
{
     char *id;
     id = (char *) ptr;
     while(true) {
       printf("#%i from thread reader \n", shared, id);
       sleep(1);
     }
}

void *incrementer( void *ptr )
{
     char *id;
     id = (char *) ptr;
     while(true) {
       shared++;
       sleep(1);
     }
}
