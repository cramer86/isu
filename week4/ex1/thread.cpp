#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
void *print_message_function( void *ptr );

main()
{
     pthread_t thread1, thread2;
     const char *id0 = "0";
     const char *id1 = "1";
     int  iret1, iret2;

     iret1 = pthread_create( &thread1, NULL, print_message_function, (void*) id0);
     iret2 = pthread_create( &thread2, NULL, print_message_function, (void*) id1);

     pthread_join( thread1, NULL);
     pthread_join( thread2, NULL); 
     exit(0);
}

void *print_message_function( void *ptr )
{
     char *id;
     id = (char *) ptr;
     for(int i = 0; i<10; i++) {
       printf("#%i from thread %s \n", i, id);
       sleep(1);
     }
}
