#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include "Vector.hpp"

void *reader( void *ptr );
void *incrementer( void *ptr );

Vector *vector = new Vector();

int sleepTime = 10000000;

main(int argc, const char* argv[])
{
     //get args
     sleepTime = atoi(argv[1]);
     printf("time to sleep: %i\n", sleepTime);
     pthread_t thread1, thread2;
     const char *id1 = "1";
     int  iret1, iret2;
     int size = 100;
     int id[size];
     pthread_t threads[size];

     for(int i = 0; i < size; i++) {
       id[i] = i;
       iret2 = pthread_create( &threads[i], NULL, incrementer, (void*) id[i]);
     }

     for(int i = 0; i < size; i++) {
       pthread_join( threads[i], NULL);
     }
     exit(0);
}

void *incrementer( void *ptr )
{
     int *id;
     id = (int *) ptr;
     for(int i = 0; true; i++){
       if(!vector->setAndTest(i)) {
	  printf("thread %i has detected and error\n", id);
	 usleep(sleepTime);
       }
     }
}
